import 'package:dartz/dartz.dart';

abstract class AuthRepository {
  Future<Either<Fail, bool>> signInWithPhoneNumber({
    required String phone,
    required String countryCode,
  });

  Future<Either<Fail, void>> savePhoneNumbre({
    required String phone,
    required String countryCode,
  });
}
