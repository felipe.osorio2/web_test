import 'package:dartz/dartz.dart';
import '../repositories/auth_repository.dart';

class VerifyPhoneNumberUseCase {
  final AuthRepository _authRepository;

  VerifyPhoneNumberUseCase(
    this._authRepository,
  );

  Future<Either<Fail, void>> call({
    required String phone,
    required String countryCode,
  }) async {
    return await _authRepository.signInWithPhoneNumber(
      phone: phone,
      countryCode: countryCode,
    );
  }
}
