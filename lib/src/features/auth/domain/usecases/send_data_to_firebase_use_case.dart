import 'package:dartz/dartz.dart';
import '../repositories/auth_repository.dart';

class SendDataToFirebaseUseCase {
  final AuthRepository _authRepository;

  SendDataToFirebaseUseCase(
    this._authRepository,
  );

  Future<Either<Fail, void>> call({
    required String phone,
    required String countryCode,
  }) async {
    return await _authRepository.savePhoneNumbre(
      phone: phone,
      countryCode: countryCode,
    );
  }
}
