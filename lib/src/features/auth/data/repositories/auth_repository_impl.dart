import 'dart:async';

import 'package:dartz/dartz.dart';
import '../services/auth_service.dart';
import '../../domain/repositories/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthService _service;

  AuthRepositoryImpl(
    this._service,
  );

  @override
  Future<Either<Fail, bool>> signInWithPhoneNumber({
    required String phone,
    required String countryCode,
  }) async {
    try {
      return Right(
        await _service.signInWithPhoneNumber(
          phone: phone,
          countryCode: countryCode,
        ),
      );
    } catch (e) {
      return Left(
        Fail(
          e.toString(),
        ),
      );
    }
  }

  @override
  Future<Either<Fail, void>> savePhoneNumbre({
    required String phone,
    required String countryCode,
  }) async {
    try {
      return Right(
        await _service.savePhoneNumbre(
          phone: phone,
          countryCode: countryCode,
        ),
      );
    } catch (e) {
      return Left(
        Fail(
          e.toString(),
        ),
      );
    }
  }
}
