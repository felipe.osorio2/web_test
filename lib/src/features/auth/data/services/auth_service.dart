// ignore_for_file: unused_field, unnecessary_null_comparison

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../../../core/utils/common.dart';

abstract class AuthService {
  Future<bool> signInWithPhoneNumber({
    required String phone,
    required String countryCode,
  });
  Future<void> savePhoneNumbre({
    required String phone,
    required String countryCode,
  });
}

class AuthServiceImpl implements AuthService {
  final FirebaseAuth _auth;
  final FirebaseFirestore _database;
  AuthServiceImpl(
    this._auth,
    this._database,
  );

  @override
  Future<bool> signInWithPhoneNumber({
    required String phone,
    required String countryCode,
  }) async {
    final response = await _auth.signInWithPhoneNumber("$countryCode$phone");
    return response != null;
  }

  @override
  Future<void> savePhoneNumbre({
    required String phone,
    required String countryCode,
  }) async {
    await _database.collection(users).doc().set({
      "phone": "$countryCode$phone",
    });
  }
}
