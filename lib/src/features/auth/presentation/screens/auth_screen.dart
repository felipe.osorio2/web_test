import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../../core/utils/button_style.dart';
import '../../../../core/utils/colors.dart';
import '../../../../core/utils/text_form_field_style.dart';
import '../../../../core/utils/validations.dart';
import '../providers/auth_provider.dart';
import '../widgets/app_bar_widget.dart';

class AuthScreen extends ConsumerStatefulWidget {
  const AuthScreen({super.key});

  @override
  ConsumerState<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends ConsumerState<AuthScreen> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final notifier = ref.read(authNotifierProvider.notifier);
    return Scaffold(
      appBar: const PreferredSize(
        preferredSize: Size(double.infinity, 60),
        child: AppBarWidget(),
      ),
      body: Form(
        key: _formKey,
        child: LayoutBuilder(builder: (context, constraints) {
          if (constraints.maxWidth < 800) {
            return _FormWidget(
              formKey: _formKey,
              notifier: notifier,
            );
          }
          return Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  color: UIColors.primaryPurple,
                ),
              ),
              Expanded(
                flex: 1,
                child: _FormWidget(
                  formKey: _formKey,
                  notifier: notifier,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}

class _FormWidget extends StatelessWidget {
  const _FormWidget({
    required this.notifier,
    required this.formKey,
  });

  final AuthNotifier notifier;
  final GlobalKey<FormState> formKey;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(size.width * 0.05),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            width: double.infinity,
            child: Text(
              'Ingresa tu número de celular',
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 25.0,
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Row(
            children: [
              Container(
                height: 60,
                margin: const EdgeInsets.only(right: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      offset: const Offset(0, 2),
                      blurRadius: 4,
                    ),
                  ],
                ),
                child: CountryCodePicker(
                  boxDecoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  textStyle: const TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                  initialSelection: 'CO',
                  onChanged: (country) =>
                      notifier.onCountryCodeChanged(country.dialCode),
                ),
              ),
              Expanded(
                child: Container(
                  height: 60,
                  margin: const EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        offset: const Offset(0, 2),
                        blurRadius: 4,
                      ),
                    ],
                  ),
                  child: TextFormField(
                    controller: notifier.phoneController,
                    style: const TextStyle(
                      fontSize: 20.0,
                      color: Colors.black,
                    ),
                    onChanged: notifier.onPhoneNumberChanged,
                    keyboardType: TextInputType.phone,
                    decoration: TextFromFieldCustomStyle.primary(
                      hintText: 'Número de celular',
                    ),
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(10),
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    validator: Validations.validatePhone,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20.0),
          SizedBox(
            width: double.infinity,
            height: 60,
            child: ElevatedButton(
              style: ButtonStyleUtils.primary,
              onPressed: () async {
                if (formKey.currentState?.validate() ?? false) {
                  FocusScope.of(context).requestFocus(FocusNode());
                  notifier
                    ..sendDataToFirebaseUseCase()
                    ..verifyPhoneNumber();
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: const Text('Felicidades'),
                        content: const Text(
                            'Tu número de celular ha sido verificado'),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text('Aceptar'),
                          ),
                        ],
                      );
                    },
                  );
                }
              },
              child: const Text(
                'Continuar',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
