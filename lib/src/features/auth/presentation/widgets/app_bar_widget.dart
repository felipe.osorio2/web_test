import 'package:flutter/material.dart';

import '../../../../core/utils/colors.dart';

class AppBarWidget extends StatelessWidget {
  const AppBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 2),
            blurRadius: 4,
          ),
        ],
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      height: 95,
      child: Row(
        children: [
          TextButton(
            child: const Row(
              children: [
                Icon(
                  Icons.arrow_back_ios,
                  color: UIColors.primaryPurple,
                  size: 17,
                ),
                SizedBox(width: 15),
                Text(
                  'Atrás',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
