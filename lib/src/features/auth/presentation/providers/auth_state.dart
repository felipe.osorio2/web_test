class AuthState {
  final bool loading;
  final String countryCode;
  final String phoneNumber;

  const AuthState({
    this.loading = false,
    this.countryCode = '+57',
    this.phoneNumber = '',
  });

  AuthState copyWith({
    bool? loading,
    String? countryCode,
    String? phoneNumber,
  }) {
    return AuthState(
      loading: loading ?? this.loading,
      countryCode: countryCode ?? this.countryCode,
      phoneNumber: phoneNumber ?? this.phoneNumber,
    );
  }
}
