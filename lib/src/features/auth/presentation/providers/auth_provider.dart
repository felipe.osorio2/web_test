// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../core/utils/validations.dart';
import '../../data/repositories/auth_repository_impl.dart';
import '../../data/services/auth_service.dart';
import '../../domain/repositories/auth_repository.dart';
import '../../domain/usecases/send_data_to_firebase_use_case.dart';

import '../../domain/usecases/verify_phone_number_use_case.dart';
import 'auth_state.dart';

final authServiceProvider = Provider<AuthService>(
  (ref) {
    return AuthServiceImpl(
      FirebaseAuth.instance,
      FirebaseFirestore.instance,
    );
  },
);

final sendDataToFirebaseUseCase = Provider<SendDataToFirebaseUseCase>((ref) {
  final repository = ref.read(authRepositoryProvider);
  return SendDataToFirebaseUseCase(repository);
});
final verifyPhoneNumberUseCase = Provider<VerifyPhoneNumberUseCase>((ref) {
  final repository = ref.read(authRepositoryProvider);
  return VerifyPhoneNumberUseCase(repository);
});
final authRepositoryProvider = Provider<AuthRepository>((ref) {
  final service = ref.read(authServiceProvider);
  return AuthRepositoryImpl(service);
});

final authNotifierProvider = StateNotifierProvider<AuthNotifier, AuthState>(
  (ref) {
    final _sendDataToFirebaseUseCase = ref.read(sendDataToFirebaseUseCase);
    final _verifyPhoneNumberUseCase = ref.read(verifyPhoneNumberUseCase);
    return AuthNotifier(
      _sendDataToFirebaseUseCase,
      _verifyPhoneNumberUseCase,
    );
  },
);

class AuthNotifier extends StateNotifier<AuthState> {
  final SendDataToFirebaseUseCase _sendDataToFirebaseUseCase;
  final VerifyPhoneNumberUseCase _verifyPhoneNumberUseCase;
  final TextEditingController phoneController = TextEditingController();
  AuthNotifier(
    this._sendDataToFirebaseUseCase,
    this._verifyPhoneNumberUseCase,
  ) : super(
          const AuthState(),
        );

  void onPhoneNumberChanged(
    String phone,
  ) {
    if (Validations.validatePhone(phone) == null) {
      state = state.copyWith(
        phoneNumber: phone,
      );
    }
  }

  void onCountryCodeChanged(
    String? countryCode,
  ) {
    if (countryCode != null) {
      state = state.copyWith(
        countryCode: countryCode,
      );
    }
  }

  Future<void> sendDataToFirebaseUseCase() async {
    phoneController.clear();
    state = state.copyWith(
      loading: true,
    );
    final result = await _sendDataToFirebaseUseCase.call(
      phone: state.phoneNumber,
      countryCode: state.countryCode,
    );
    state = state.copyWith(
      loading: false,
    );
    result.fold(
      (fail) {
        print("fail");
      },
      (success) {
        print("success");
      },
    );
  }

  Future<void> verifyPhoneNumber() async {
    phoneController.clear();
    state = state.copyWith(
      loading: true,
    );
    final result = await _verifyPhoneNumberUseCase.call(
      phone: state.phoneNumber,
      countryCode: state.countryCode,
    );
    state = state.copyWith(
      loading: false,
    );
    result.fold(
      (fail) {
        print("fail");
      },
      (success) {
        print("success");
      },
    );
  }
}
