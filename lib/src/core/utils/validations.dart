class Validations {
  static String? validatePhone(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ingresa el número de celular';
    }
    if (value.length < 8) {
      return 'Ingresa un número de celular válido';
    }
    return null;
  }
}
