import 'package:flutter/material.dart';

import 'colors.dart';

class ButtonStyleUtils {
  static final primary = ElevatedButton.styleFrom(
    backgroundColor: UIColors.primaryPurple,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
  );
}
