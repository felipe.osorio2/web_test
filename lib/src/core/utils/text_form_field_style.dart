import 'package:flutter/material.dart';

class TextFromFieldCustomStyle {
  static InputDecoration primary({
    required String hintText,
  }) {
    return InputDecoration(
      hintText: hintText,
      counterStyle: const TextStyle(
        fontSize: 0,
      ),
      hintStyle: const TextStyle(
        fontSize: 20.0,
        color: Color.fromARGB(255, 203, 203, 203),
      ),
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 15.0,
      ),
      border: InputBorder.none,
    );
  }
}
