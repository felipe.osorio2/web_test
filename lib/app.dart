import 'package:flutter/material.dart';

import 'src/core/utils/fonts.dart';
import 'src/features/auth/presentation/screens/auth_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Auth Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: Fonts.urban,
      ),
      home: const AuthScreen(),
    );
  }
}
